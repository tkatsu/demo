package com.example.demo.repository;

import com.example.demo.entity.GoodsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tkatsuma on 2017/07/12.
 */
@Repository
public interface GoodsRepository extends JpaRepository<GoodsEntity, Long> {

}
