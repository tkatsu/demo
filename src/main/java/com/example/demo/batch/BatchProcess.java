package com.example.demo.batch;

import com.example.demo.entity.GoodsEntity;
import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by tkatsuma on 2017/07/12.
 */
@Component
public class BatchProcess {
    @Autowired
    private TestService testService;
    private int count = 0;


    @Scheduled(initialDelay = 0, fixedDelay = 5000)
    public void run() {
        count++;
        String countStr = String.valueOf(count);
        // APIをたたく処理
        System.out.println(testService.getZipcodeFromApi("184-000"+countStr));

        // DBにGoodを登録処理
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setName("good"+countStr);
        goodsEntity.setPrice(count*100);
        testService.saveGoods(goodsEntity);
    }

}
