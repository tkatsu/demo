package com.example.demo.entity;


import javax.persistence.*;

/**
 * Created by tkatsuma on 2017/07/12.
 *
 * Table name   : goods
 * field1       : id INT PRIMARY KEY AUTO_INCREMENT
 * field2       : name Varchar
 * field3       : price INT
 */
@Entity
@Table(name = "goods")
public class GoodsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
