package com.example.demo.service;

import com.example.demo.entity.GoodsEntity;
import com.example.demo.repository.GoodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by tkatsuma on 2017/07/12.
 */
@Service
@Qualifier("zipCodeSearchRestTemplate")
public class TestService {
    @Autowired
    GoodsRepository goodsRepository;
    private static final String ZIPURL = "http://zipcloud.ibsnet.co.jp/api/search?zipcode=%s";


    public String getYeah() {
        return "Yeahhhhhhhhh";
    }

    public String getZipcodeFromApi(String zipCode) {
        RestTemplate restTemplate = new RestTemplate();
        String expansionZipUrl = String.format(ZIPURL, zipCode);
        return restTemplate.getForObject(expansionZipUrl, String.class);
    }

    public GoodsEntity saveGoods(GoodsEntity goodsEntity) {
        return goodsRepository.save(goodsEntity);
    }
}
